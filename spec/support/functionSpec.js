const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const data = require("../../resources/Data.json");
input = "USA";

describe('chatbot', () => {
  it("Should have a length of 1", () => {
    userSelection = [];
    for (let i = 0; i < data.length; i++) {
      let obj = data[i];
      if (obj.Country == input) {
        userSelection.push(input);
      }
    }
    expect(userSelection.length).toBeGreaterThanOrEqual(2);
  });

  it("should add html", () => {
    let temp = `<div class="inbound-message">        <img src="/resources/Userimg1.jpg" class="pic">        <span class="message">Please enter a valid Response</span>        </div>`;
    let dom = new JSDOM(`<!DOCTYPE html><div></div>`);
    let div = dom.window.document.querySelector("div");
    div.innerHTML = temp;
    expect(div.innerHTML) == temp;
  })
});
