const popup = document.querySelector('.chat-popup');
const chatBtn = document.querySelector('.chat-bttn');
const submit = document.querySelector('.submit');
const chatArea = document.querySelector('.chat-area');
const input = document.querySelector('input');
const holRec = document.querySelector('.holiday-rec');

const userSelection = [];


//Gets all Holiday data 
function fetchrequest() {
    fetch('/resources/Data.json', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
        },
    })
        .then(response => response.json())
        .then(data => validateData(data, input.value))

}

//Validates Country user input
function validateData(data, input) {
    for (let i = 0; i < data.length; i++) {
        let obj = data[i];
        if (obj.Country == input) {
            userSelection.push(input);
            setTimeout(() => {
                let temp = `<div class="inbound-message">
        <img src="/resources/Userimg1.jpg" class="pic">
        <span class="message">What is your budget Per Person?</span>
        </div>`;
                chatArea.insertAdjacentHTML("beforeend", temp);
            }, 1000);
            break;
        }
        if (parseInt(input)) {
            userSelection.push(input);
            setTimeout(() => {
                let temp = `<div class="inbound-message">
                <img src="/resources/Userimg1.jpg" class="pic">
                <span class="message">Loading Results...</span>
                </div>`;
                chatArea.insertAdjacentHTML("beforeend", temp);
            }, 1000);
            displayData(data)
            break;
        }
    }
    if (!userSelection.includes(input)) {
        let temp = `<div class="inbound-message">
        <img src="/resources/Userimg1.jpg" class="pic">
        <span class="message">Please enter a valid Response</span>
        </div>`;
        chatArea.insertAdjacentHTML("beforeend", temp);
    }
}

function displayData(data) {
    let match = false;
    for (let i = 0; i < data.length; i++) {
        let obj = data[i];
        if (obj.Country == userSelection[0] && obj.PricePerPerNight <= userSelection[1]) {
            match = true;
            setTimeout(() => {
                let temp = `
            <div class="container">
                <div>
                    <img src="/resources/work-vacation.jpg" class="listingprofile">
                </div>
                <div class="listings">
                    Hotel: ${obj.HotelName}<br>
                    City: ${obj.City}<br>
                    Country: ${obj.Country}<br>
                    Rating: ${obj.StarRating}<br>
                    Price PP: £${obj.PricePerPerNight}<br>
                    <button class="package-btn">View Hotel</button>
                </div>
            </div>`;
                holRec.insertAdjacentHTML("beforebegin", temp);

            }, 2000);
        }
    }

    if (!match) {
        setTimeout(() => {
            let temp = `
    <div class="container">
    <div class="listings">
        No Holidays Found
    </div>
    </div>
    `
            holRec.insertAdjacentHTML("beforebegin", temp);
        }, 2000);
    }
}



//Show/Hide chat box
chatBtn.addEventListener('click', () => {
    popup.classList.toggle('display');
});


//User sends message
submit.addEventListener('click', () => {
    let userInput = input.value;
    fetchrequest()
    let temp = `<div class="output-msg">
    <span class="usr-msg">${userInput}</span>
    <img src="/resources/Userimg1.jpg" class="pic">
    </div>`;
    chatArea.insertAdjacentHTML("beforeend", temp);
})

